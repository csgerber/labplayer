package edu.uchicago.gerber.labplayer;


import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.IBinder;

import java.util.HashMap;
import java.util.Map;


//http://www.truiton.com/2014/11/bound-service-example-android/
public class BackgroundSoundService extends Service {
    private static final String TAG = null;
    private MediaPlayer player;
    private int volume = 100;
    private IBinder mBinder = new MyBinder();

    private double currentTime = 0;
    private double endTime = 0;




    @Override
    public void onCreate() {
        super.onCreate();
        player = MediaPlayer.create(this, R.raw.piano);
        endTime = player.getDuration();
        currentTime = player.getCurrentPosition();

        player.setVolume(volume,volume);



    }
    public int onStartCommand(Intent intent, int flags, int startId) {

        return Service.START_STICKY;
    }

    public void onStart(Intent intent, int startId) {
        // TO DO
    }
    public IBinder onUnBind(Intent arg0) {
        // TO DO Auto-generated method
        return null;
    }

    public Map<String, Double> getCurrentSeek(){
        Map map = new HashMap();
        map.put("current", (double) player.getCurrentPosition());
        map.put("end", (double) player.getDuration());

        return map;
    }

    public void onStop() {

    }
    public void onPause() {

    }

    public void decrementVolume(){
        if (volume >=10) {
            volume -= 10;
        }

        player.setVolume(volume, volume);
    }

    public void incrementVolume(){
        if (volume <=90) {
            volume += 10;
        }

        player.setVolume(volume, volume);
    }

    public void playerPause(){
        player.pause();
    }

    public void playerPlay(){
        player.start();
        player.setLooping(true);
    }
    @Override
    public IBinder onBind(Intent intent) {

        return mBinder;
    }

    @Override
    public void onRebind(Intent intent) {

        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {

        return true;
    }


    @Override
    public void onDestroy() {
        player.stop();
        player.release();
    }

    @Override
    public void onLowMemory() {

    }

    public class MyBinder extends Binder {
        BackgroundSoundService getService() {
            return BackgroundSoundService.this;
        }
    }
}