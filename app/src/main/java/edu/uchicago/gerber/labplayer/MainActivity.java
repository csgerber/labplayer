package edu.uchicago.gerber.labplayer;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.SeekBar;

import java.util.ArrayList;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

//https://stackoverflow.com/questions/8209858/android-background-music-service
public class MainActivity extends AppCompatActivity {


    private BackgroundSoundService mBoundService;
    private boolean isMusicPlaying = false;
    private boolean isServiceBound = false;

    private Button btnPlay, btnPause, btnRewind, btnForward;

    private SeekBar seekBar;

    private Timer timer;
    private Map<String, Double> mapSeeks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        seekBar = findViewById(R.id.seekBar);
        //seekBar.setProgress(50);

        timer = new Timer();


        btnPlay = findViewById(R.id.btnPlay);
        btnPause = findViewById(R.id.btnPause);
        btnRewind = findViewById(R.id.btnRewind);
        btnForward = findViewById(R.id.btnForward);

        btnPlay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //startUpSeek();
                new SeekTask().execute();

                mBoundService.playerPlay();


            }
        });

        btnPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBoundService.playerPause();
            }
        });

        btnRewind.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBoundService.decrementVolume();
            }
        });

        btnForward.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mBoundService.incrementVolume();
            }
        });


    }

    @Override
    protected void onPause() {
        if (isServiceBound) {
            unbindService(mServiceConnection);
            isServiceBound = false;

        }
        timer.cancel();
        super.onPause();
    }

    @Override
    protected void onResume() {
        Intent intent = new Intent(MainActivity.this, BackgroundSoundService.class);
        startService(intent);
        bindService(intent, mServiceConnection, Context.BIND_AUTO_CREATE);

        // startUpSeek();


        super.onResume();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceDisconnected(ComponentName name) {
            isServiceBound = false;
        }

        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            BackgroundSoundService.MyBinder myBinder = (BackgroundSoundService.MyBinder) service;
            mBoundService = myBinder.getService();
            isServiceBound = true;
        }
    };


    private class SeekTask extends AsyncTask<Void, Integer, Void> {
        @Override
        protected void onProgressUpdate(Integer... values) {
            seekBar.setProgress(values[0]);
            super.onProgressUpdate(values);
        }

        @Override
        protected Void doInBackground(Void... voids) {
            if (isServiceBound) {
                timer.schedule(new TimerTask() {

                    @Override
                    public void run() {
                        mapSeeks = mBoundService.getCurrentSeek();
                        onProgressUpdate((int) (((mapSeeks.get("current") / mapSeeks.get("end")) * 10000)));
                    }
                }, 200);
            }
            return null;
        }


    }
}
